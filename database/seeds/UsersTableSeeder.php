<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create()->each(function ($u) {
            $u->roles()->attach(App\Role::ROLE_ADMINISTRATOR);
        });

        factory(App\User::class, 5)->create()->each(function ($u) {
            $u->roles()->attach(App\Role::ROLE_STUDENT);
        });
    }
}
