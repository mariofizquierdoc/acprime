<?php

use Illuminate\Database\Seeder;

class PopulateRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Student',
        ]);
        DB::table('roles')->insert([
            'name' => 'Administrator',
        ]);
    }
}
