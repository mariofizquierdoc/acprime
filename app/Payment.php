<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'payment_type_id', 'course_id', 'amount', 'identifier', 'verified',
    ];

    public function paymentType()
    {
        return $this->belongsTo('App\PaymentType');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
