<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseSession extends Model
{
    protected $fillable = [
        'course_id', 'start_at', 'end_at',
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
