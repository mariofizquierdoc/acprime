<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'course_type_id', 'term_id', 'name', 'start_at', 'end_at', 'price',
    ];

    public function courseType()
    {
        return $this->belongsTo('App\CourseType');
    }

    public function term()
    {
        return $this->belongsTo('App\Term');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function topics()
    {
        return $this->hasMany('App\Topic');
    }

    public function courseSessions()
    {
        return $this->hasMany('App\CourseSession');
    }
    
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }
}
