<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = [
        'year', 'term_number',
    ];

    public function courses()
    {
        return $this->hasMany('App\Course');
    }
}
