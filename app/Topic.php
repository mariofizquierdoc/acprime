<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'course_id',  'name',  'duration',
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
